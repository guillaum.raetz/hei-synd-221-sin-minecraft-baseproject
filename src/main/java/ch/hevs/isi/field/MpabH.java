package ch.hevs.isi.field;

import java.nio.ByteBuffer;

/**
 * Class for the MBPA Header
 */
public class MpabH {

    private int transactionID;
    private int protocolID;
    private int length;
    private ByteBuffer theBB = ByteBuffer.allocate(7);
    private byte unitID;

    /**
     * Header used for transfer of modbus frame over TCP/IP
     * @param tID : Identification of a Modbus Request / Response transaction
     * @param RTU : Id of a remote slave connected on a serial line or on other buses
     * @param length : Number of following bytes
     */
    public MpabH(int tID,int RTU,int length){
        this.transactionID = tID;
        this.protocolID = 0;//0 = modbus protocol
        this.length = length;
        this.unitID = (byte) RTU;
        theBB.putShort((short) transactionID);
        theBB.putShort((short) protocolID);
        theBB.putShort((short) length);
        theBB.put(unitID);
    }

    public MpabH(byte[] b){
        //transform bytebuffer in Mpabh to read data
    }

    /**
     * Simple get function for the bytebuffer
     * @return a byterbuffer
     */
    public byte[] get(){
        return theBB.array();
    }

}
