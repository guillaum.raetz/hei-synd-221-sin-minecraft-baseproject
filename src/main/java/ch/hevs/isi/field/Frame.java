package ch.hevs.isi.field;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/**
 * Class for modbus frame
 * includes a MpabH, a pdu and a bytebuffer
 */
public class Frame {
    private MpabH FrameMbapH;
    private Pdu pdu;
    private ByteBuffer theBB;
    private int size = 0;

    public void initPdu(){

    }

    /**
     * Constructor that creates the ByteBuffer
     * @param size : size of the ByteBuffer
     */
    public Frame(int size){
        this.size = size;
        theBB = ByteBuffer.allocate(size);
    }

    public void getPdu(){

    }

    /**
     * Send the frame on the "Outputstream"
     * @param os : Outputstream where we will send the frame
     */
    public void send(OutputStream os) throws IOException {
        byte[] frameToSend = new byte[size];
       // theBB.get(frameToSend);
        os.write(theBB.array());
    }

    public void receive(){

    }

    /**
     * Add the MpabH to the frame
     * @param mbaph : mbaph to be added
     */
    public void put(MpabH mbaph) {
       theBB.put(mbaph.get());
    }

    /**
     * Add the Pdu to the frame
     * @param pdu : pdu to be added
     */
    public void put(Pdu pdu) {
        theBB.put(pdu.get());
    }

}
