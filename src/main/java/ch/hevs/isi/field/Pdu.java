package ch.hevs.isi.field;

import java.nio.ByteBuffer;

/**
 * Abstract class to create PDU, parent class of the different type of PDU(BooleanWriteRequest, FloatReadRequest,...)
 */
public abstract class Pdu {
    protected int functionCode;
    protected ByteBuffer theBB;

    /**
     * Constructor that initialize the function code and the length of the bytebuffer
     * @param functionCode : function code for modbus
     * @param size : size of the bytebuffer
     */
    public Pdu(int functionCode,int size){
        this.functionCode = functionCode;
        theBB = ByteBuffer.allocate(size);
    }

    public void set(){

    }

    /**
     * Simple getter for the byteBuffer
     * @return ByteBuffer
     */
    public byte[] get(){
        return theBB.array();
    }
}
