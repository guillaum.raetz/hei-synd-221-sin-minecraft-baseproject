package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.utils.Utility;

import javax.rmi.CORBA.Util;
import javax.xml.crypto.Data;
import java.sql.Time;
import java.util.Timer;
import java.util.TimerTask;


import java.io.IOException;


/**
 * Used to poll all the values of the Minecraft world inside datapoints
 * Links Register to Datapoints
 */
public class FieldConnector implements DataPointListener{

    private static FieldConnector fieldCon;
    static String modbusHost;
    static int modbusPort;

    private Timer pollTimer = null;

    /**
     * Timer that launch a function each ? ms (We can define the time)
     */
    private TimerTask pollTask = new TimerTask() {
        @Override
        public void run() {
            try {
                poll();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Configuration of the modbus
     * @param address : address of the modbus
     * @param port : port of the modbus
     */
    public static void config(String address, int port){
        modbusHost = address;
        modbusPort = port;
    }

    /**
     * Getter for an instance of FieldConnector, if there is none we create one
     * @return the instance of FieldConnector
     */
    public static FieldConnector get(){
        if (fieldCon == null)
        {
            fieldCon = new FieldConnector();
        }
        return fieldCon;
    }

    /**
     * Called by a timer each ? ms
     * Read and write all the modbusregisters
     */
    public void poll() throws IOException {
        Utility.DEBUG("fieldConnector","poll","Polling");

        for(ModbusRegister modReg : ModbusRegister.getRegisters()){
            modReg.read();
            modReg.write();
        }
    }

    /**
     * Constructor that creates a new timer and set it to ticks each 1000 ms
     */
    public FieldConnector(){
        Utility.DEBUG("fieldConnector","main","Constructor fieldconnector");
        pollTimer = new Timer();
        pollTimer.scheduleAtFixedRate(pollTask,0,1000);
    }

    /**
     * When the value is changed, we write the new value on the minecraft world thanks to the modbus
     * @param dataPoint
     */
    public void onNewValue(DataPoint dataPoint) throws IOException {
        ModbusRegister modReg = ModbusRegister.getRegFromDP(dataPoint);
        if(modReg != null) {
            if (modReg.isBinary()) {
                ModbusAccessor.get().writeBoolean(modReg.getAdress(), Boolean.parseBoolean(dataPoint.getValue()));
            } else {
                ModbusAccessor.get().writeFloat(modReg.getAdress(),Float.parseFloat(dataPoint.getValue()));
            }
        }
    }

    /**
     * Create a new ModbusRegister
     * @param label : name of the register
     * @param address : address on the minecraft world
     * @param range : Range of the value
     * @param offset : offset of the value
     * @param isBinary : is it binary ?
     * @param isOutput : is it an output ?
     */
    public void addModbusReg(String label, int address,int range, int offset, boolean isBinary, boolean isOutput){
        if(isBinary == true){
            new BooleanRegister(isOutput,address,label);
        }
        else {
            new FloatRegister(address,label,isOutput,range,offset);
        }
    }
}
