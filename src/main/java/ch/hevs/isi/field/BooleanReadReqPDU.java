package ch.hevs.isi.field;

/**
 * Pdu class for a modbus boolean request
 */
public class BooleanReadReqPDU extends Pdu {
    /**
     * Constructor for the pdu
     * We call the super constructor as this is a child class
     * The function code is 0x01, we need to put the address too
     * @param address : address of the register we want to read
     */
    public BooleanReadReqPDU(int address){
        super(0x01,5);
        theBB.put((byte) functionCode);
        theBB.putShort((short)address);
        theBB.putShort((short) 1);
    }
}
