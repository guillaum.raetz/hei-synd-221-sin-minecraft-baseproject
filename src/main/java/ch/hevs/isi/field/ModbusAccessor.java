package ch.hevs.isi.field;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.utils.Utility;
import ch.hevs.isi.smartcontrol.smartcontrol;
import ch.hevs.isi.web.WebConnector;
import com.sun.org.apache.xpath.internal.operations.Mod;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import ch.hevs.isi.field.FieldConnector;

/**
 * Main class of our project
 * It is used to deploy the modbus component
 */
public class ModbusAccessor extends Socket {
    private int transaction_ID;
    private final static int RTU_ADRESS = 1;
    private byte[] readBuff;
    private OutputStream os;
    private InputStream is;
    private static ModbusAccessor mb;

    /**
     * Constructor
     * This class extends "Socket" so we need to call its constructor with the "address" and "port"
     * @param address : address of the modbus server
     * @param port : port of the modbus server
     */
    private ModbusAccessor(String address,int port) throws IOException {
        super(address,port);//Connect to tcp/ip server
        os = super.getOutputStream();//Get the input and output stream of the
        is = super.getInputStream();
        this.transaction_ID = 0;
    }

    /**
     * Get the instance of the ModbusAccessor
     * If there is none we create one
     * @return the instance of ModbusAccessor
     */
    public static ModbusAccessor get() throws IOException {
        if(mb == null){
            mb = new ModbusAccessor("localhost",1502);
        }
        return mb;
    }

    /**
     * Read a CSV file that contains all the values of the minecraft world
     * Create all the correspodning datapoints and modbusRegisters
     */
    public static void readCSV() throws IOException {
            BooleanRegister booleanRegister = null;
            FloatRegister floatRegister = null;
            String path = "ModbusEA_SIn.csv";
            String data = "";
            BufferedReader reader = Utility.fileParser(null, path);
            boolean isOutput = false;

        while (reader.ready()) {
                data = reader.readLine();
                String[] array = data.split(";");

                if (array[4] == "Y") {
                    isOutput = true;
                }
                if (array[0].contains("SW")) {//If is boolean
                    System.out.println(array[0] + array[5]);
                    BinaryDataPoint bp = new BinaryDataPoint(array[0], isOutput);
                    booleanRegister = new BooleanRegister(isOutput,Integer.parseInt(array[5]), (String)array[0]);
                    booleanRegister.addDataPoint(bp);
                }
                else {
                    System.out.println(array[0] + array[5]);
                    FloatDataPoint fp = new FloatDataPoint(array[0], isOutput);
                    floatRegister = new FloatRegister(Integer.parseInt(array[5]), (String) array[0], isOutput, Integer.parseInt(array[6]), Integer.parseInt(array[7]));
                    floatRegister.addDataPoint(fp);
                }
        }
    }

    /**
     * Main class of our project
     * Call the readCSV method first to instanciate all the datapoints
     * Creates instance of FieldConnector, smartControl, webConnector and dataBaseConnector
     * Infinite loop
     * @param args
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        mb = new ModbusAccessor("localhost",1502);

        readCSV();

        FieldConnector fdc = new FieldConnector();
        smartcontrol smartC = new smartcontrol();

        WebConnector wc = WebConnector.getInstance();
        DatabaseConnector db = DatabaseConnector.getInstance();


        while(true){
        //    smartC.smartControlling();

            // boolean value = mb.readBooleanReq(613);
            //Utility.DEBUG("modbusAcessor","main","value of windturbines : "+value);
            //value = mb.readBooleanReq(609);

//            mb.writeBoolean(401,true);
           // mb.writeFloat(205,0.1f);
            //mb.writeFloat(209,0.99f);

            Thread.sleep(500);
            //float val = mb.readFloat(601);
            //Utility.DEBUG("modbusAcessor","main","Coal : "+val);
  //          mb.writeBoolean(405,true);
            //mb.writeFloat(205,0.5f);


        }
    }

    /**
     * Read a float on the corresponding address in the minecraft world thank to modbus
     * @param address : address of the register to read
     * @return value of the register
     */
    public float readFloat(int address) {
        Frame req_frame = new Frame(12);
        MpabH mbaph = new MpabH(transaction_ID,RTU_ADRESS,6);
        req_frame.put(mbaph);
        FloatReadReqPDU pdu = new FloatReadReqPDU(address);
        req_frame.put(pdu);
        float response = 0;
        try {
            req_frame.send(os);


            byte[] resp = Utility.readNBytes(is, 7);
            if (resp != null && resp.length == 7) {//If the the reponse is the correct format
                ByteBuffer r = ByteBuffer.wrap(resp);
                int protocolID = r.getShort(2);
                int lenRespPdu = r.getShort(4);
                int unitID = r.get(6);

                byte[] respPdu = Utility.readNBytes(is, lenRespPdu - 1);

                ByteBuffer bbPdu = ByteBuffer.wrap(respPdu);
                int functionCode = bbPdu.get(0);
                int byteCount = bbPdu.get(1);
                response = bbPdu.getFloat(2);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        transaction_ID++;

        return response;
    }

    /**
     * Write a boolean on the corresponding address in the minecraft world thanks to modbus
     * @param address : address of the register to write
     * @param value : value to write (true, false)
     */
    public void writeBoolean(int address,boolean value) {
        Frame req_frame = new Frame(12);
        MpabH mbaph = new MpabH(transaction_ID,RTU_ADRESS,6);
        req_frame.put(mbaph);
        BooleanWriteReqPDU pdu = new BooleanWriteReqPDU(address,value);
        req_frame.put(pdu);
        try {
            req_frame.send(os);

            byte[] resp = Utility.readNBytes(is, 7);
            if (resp != null && resp.length == 7) {//If the the reponse is the correct format
                ByteBuffer r = ByteBuffer.wrap(resp);
                int protocolID = r.getShort(2);
                int lenRespPdu = r.getShort(4);
                int unitID = r.get(6);

                byte[] respPdu = Utility.readNBytes(is, lenRespPdu - 1);

                ByteBuffer bbPdu = ByteBuffer.wrap(respPdu);
                int functionCode = bbPdu.get(0);//0x05 normally
                int outputAdress = bbPdu.getShort(1);
                int outputValue = bbPdu.getShort(3);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } ;

        transaction_ID++;
    }


    /**
     * Read a boolean value on the registers according to the address
     * @param address : address of the register to read
     * @return the value of the corresponding register
     */
    public boolean readBooleanReq(int address) throws IOException {
        Frame req_frame = new Frame(12);
        MpabH mpabh = new MpabH(transaction_ID,RTU_ADRESS,6);
        req_frame.put(mpabh);
        BooleanReadReqPDU pdu = new BooleanReadReqPDU(address);
        req_frame.put(pdu);
        req_frame.send(os);

        int response=0;

        //Read MBPAH Header
        byte[] resp = Utility.readNBytes(is, 7);
        if (resp != null && resp.length == 7) {//If the the reponse is the correct format
            ByteBuffer r = ByteBuffer.wrap(resp);

            int protocolID = r.getShort(2);
            int lenRespPdu = r.getShort(4);//Get the short data that correspond to the length of the pdu
            int unitID = r.get(6);


            byte[] respPdu = Utility.readNBytes(is, lenRespPdu-1);//Read the Pdu depending on length
            ByteBuffer bbPdu = ByteBuffer.wrap(respPdu);
            int functionCode = bbPdu.get(0);
            int byteCount = bbPdu.get(1);
            response = bbPdu.get(2);
        }

        //Read MPABH -> get size -> read size pdu and get data
        //PDU = function code + data
        //get size of pdu the put in in mpabh
    /*MODBUS Request PDU, mb_req_pdu
• MODBUS Response PDU, mb_rsp_pdu*/
        if(response == 1){
            return true;
        }
        if(response == 0){
            return false;
        }

        transaction_ID++;

        return false;
    }


    /**
     * Write a float on the correspond register thanks to modbus
     * @param address : address of the register
     * @param value : value to write
     */
    public void writeFloat(int address, float value)  {
        Frame req_frame = new Frame(17);
        MpabH mbaph = new MpabH(transaction_ID,RTU_ADRESS,11);
        req_frame.put(mbaph);
        FloatWriteReqPDU pdu = new FloatWriteReqPDU(address,value);
        req_frame.put(pdu);

        try {
            req_frame.send(os);

            //Read MBPAH Header
            byte[] resp = Utility.readNBytes(is, 7);
            if (resp != null && resp.length == 7) {//If the the reponse is the correct format
                ByteBuffer r = ByteBuffer.wrap(resp);

                int protocolID = r.getShort(2);
                int lenRespPdu = r.getShort(4);//Get the short data that correspond to the length of the pdu
                int unitID = r.get(6);


                byte[] respPdu = Utility.readNBytes(is, lenRespPdu - 1);//Read the Pdu depending on length
                ByteBuffer bbPdu = ByteBuffer.wrap(respPdu);
                if (respPdu.length == 5) {
                    int functionCode = bbPdu.get(0);
                    int registerAdress = bbPdu.getShort(1);
                    int quantityOfRegisters = bbPdu.getShort(3);
                    //Utility.DEBUG("main","WriteFloat","Float OK");
                } else if (respPdu.length == 2) {
                    int errorCode = bbPdu.get(0);
                    int exeptionCode = bbPdu.get(1);
                    Utility.DEBUG("main", "WriteFloat", "Float FAILED");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        transaction_ID++;
    }
}
