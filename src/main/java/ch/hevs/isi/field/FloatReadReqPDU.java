package ch.hevs.isi.field;

/**
 * Pdu class for a modbus float read request
 */
public class FloatReadReqPDU extends Pdu{
    /**
     * Constructor
     * We call the super constructor as this is a child class
     * @param address : address of the float register we want to read
     */
    public FloatReadReqPDU(int address){
        super(0x03,5);
        theBB.put((byte) functionCode);
        theBB.putShort((short)address);
        theBB.putShort((short) 2);
    }
}
