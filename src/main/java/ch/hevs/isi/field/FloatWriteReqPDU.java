package ch.hevs.isi.field;

/**
 * Pdu class for a modbus float write request
 */
public class FloatWriteReqPDU extends Pdu{
    /**
     * Constructor
     * We call the super constructor
     * @param address : address of the register we want to write
     * @param value : value we want to write on the register
     */
    public FloatWriteReqPDU(int address,float value){
        super(0x10,10);
        theBB.put((byte) functionCode);
        theBB.putShort((short)address);
        theBB.putShort((short) 2);
        theBB.put((byte) 4);
        theBB.putFloat(value);
    }
}
