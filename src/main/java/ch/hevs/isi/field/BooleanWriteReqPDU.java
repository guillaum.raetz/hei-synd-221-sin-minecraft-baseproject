package ch.hevs.isi.field;

/**
 * Class for a pdu of a modbus boolean write request
 */
public class BooleanWriteReqPDU extends Pdu{
    /**
     * Constructor for the pdu
     * We call the super constructor as this is a child class
     * Depending on what we want to write (True or False) we put a different value (0xFF00 or 0x0000)
     * @param address : address of the register we want to change
     * @param value : value we want to put in the register
     */
    public BooleanWriteReqPDU(int address,boolean value){
        super(0x05,5);
        theBB.put((byte) functionCode);
        theBB.putShort((short)address);
        if(value == true){
            theBB.putShort((short) 0xFF00);
        }
        if(value == false){
            theBB.putShort((short) 0x0000);
        }

    }
}
