package ch.hevs.isi.field;
import ch.hevs.isi.core.FloatDataPoint;
import java.io.IOException;

/**
 * Register used to store a floatDataPoint
 */
public class FloatRegister extends ModbusRegister{

    private FloatDataPoint dp;
    private int range;
    private int offset;

    static String modbusHost;
    static int modbusPort;

    /**
     * Constructor
     * @param adress : address of the register
     * @param label : name of the register
     * @param isOutput : if the specified address is an output ( for expample a switch is an output, opposed to Grid voltage which is not)
     * @param range : range used to calculate the real value
     * @param offset : offset used to calculate the real value
     */
    public FloatRegister(int adress, String label, boolean isOutput,int range,int offset){
        super(adress,false);
        dp = new FloatDataPoint(label,isOutput);
        this.offset = offset;
        this.range = range;
        this.addDataPoint(dp);
    }

    /**
     * Write the value of the datapoint to its adress in the minecraft word
     * We need to make some calculations with offset and range before, because Electrical age accepts only value between 0 - 1
     */
    public void write() throws IOException {
        ModbusAccessor.get().writeFloat(getAdress(),(Float.parseFloat(dp.getValue())-offset)/range);
    }

    /**
     * Change the value of the datapoint, according to the value we read on modbus
     * We need to make some calculations with offset and range before, because Electrical age accepts only value between 0 - 1
     */
    public void read() throws IOException {
        dp.setValue(ModbusAccessor.get().readFloat(getAdress())*range+offset);
    }

    /**
     * Configure the modbus
     * @param host : address of the server
     * @param port : port number
     */
    public void config(String host, int port){
        modbusHost = host;
        modbusPort = port;
    }
}
