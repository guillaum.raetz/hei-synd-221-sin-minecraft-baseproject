package ch.hevs.isi.field;

import com.sun.org.apache.xpath.internal.operations.Mod;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import ch.hevs.isi.core.DataPoint;

/**
 * Abstract class used as ba
 */
public abstract class ModbusRegister {
    private static Map<DataPoint,ModbusRegister> map = new HashMap<>();
    private int address;
    private boolean isBinary;

    /**
     * Constructor
     * @param address : address
     * @param isBinary : if the datapoint is binary
     */
    public ModbusRegister(int address,boolean isBinary){
        this.address = address;
        this.isBinary = isBinary;
    }

    /**
     * abstract method that will be written in child class
     */
    public abstract void read() throws IOException;

    /**
     * abstract method that will be written in child class
     */
    public abstract void write() throws IOException;

    /**
     * get the size of the map
     * @return the size of the map
     */
    public int getSize(){
        return map.size();
    }

    /**
     * Add a datapoint in the map
     * @param dataPoint : datapoint to be added
     */
    protected void addDataPoint(DataPoint dataPoint){
        map.put(dataPoint,this);
    }

    /**
     * Get a register based on its datapoint
     * @param dataPoint :
     * @return the corresponding register
     */
    protected static ModbusRegister getRegFromDP(DataPoint dataPoint){
        return map.get(dataPoint);
    }

    /**
     * Simple getter for isBinary
     * @return isBinary
     */
    public boolean isBinary(){
        return isBinary;
    }

    /**
     * Simple getter for address
     * @return address
     */
    public int getAdress() {
        return address;
    }

    /**
     * Return all the register in a "Collection"
     * @return all the registers
     */
    public static Collection<ModbusRegister> getRegisters(){
        return map.values();
    }
}
