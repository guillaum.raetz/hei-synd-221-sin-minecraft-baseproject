package ch.hevs.isi.field;
import ch.hevs.isi.core.BinaryDataPoint;
import com.sun.org.apache.xpath.internal.operations.Mod;

import java.io.IOException;

/**
 * Register used to store a binaryDataPoint
 */
public class BooleanRegister extends ModbusRegister{

    private BinaryDataPoint dp;
    static String modbusHost;
    static int modbusPort;

    /**
     * Constructor
     * @param isOutput : if the specified address is an output ( for expample a switch is an output, opposed to Grid voltage which is not)
     * @param address : adress of the register
     * @param label : name of the register
     */
    public BooleanRegister(boolean isOutput, int address, String label){
        super(address,true);
        dp = new BinaryDataPoint(label,isOutput);
        this.addDataPoint(dp);
    }

    /**
     * Used to configurate the modbus connection parameters
     * @param host : host adress
     * @param port : port number
     */
    public void config(String host, int port){
        modbusHost = host;
        modbusPort = port;
    }

    /**
     * Send the value of the boolean datapoint on the modbus server
     */
    public void write() throws IOException {
        ModbusAccessor.get().writeBoolean(getAdress(),Boolean.valueOf(dp.getValue()));
    }

    /**
     * Get the value at the adress of the register on the minecraft world
     * change the value of the boolean datapoint according the value returned by the modbus server
     */
    public void read() throws IOException {
        dp.setValue(ModbusAccessor.get().readBooleanReq(getAdress()));
    }
}
