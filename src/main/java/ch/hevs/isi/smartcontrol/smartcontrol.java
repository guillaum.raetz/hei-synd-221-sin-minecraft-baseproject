package ch.hevs.isi.smartcontrol;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.utils.Utility;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Singleton class used for the regulation of the minecraft world
 * used to adjust the setpoints according to the current world values
 * and forecast, as to get the best score possible
 */
public class smartcontrol {
    private static smartcontrol smartCon;
    private Timer timer;
    private TimerTask timerTask;

    //We make the DataPoints of each variable in the cvs file
    DataPoint Batt_p_left = DataPoint.getDataPointfromlabel("BATT_CHRG_FLOAT");
    DataPoint R_Coal_SP = DataPoint.getDataPointfromlabel("REMOTE_COAL_SP");
    DataPoint R_Factory_SP = DataPoint.getDataPointfromlabel("REMOTE_FACTORY_SP");
    DataPoint R_Solar_SW = DataPoint.getDataPointfromlabel("REMOTE_SOLAR_SW");
    DataPoint R_Wind_SW = DataPoint.getDataPointfromlabel("REMOTE_WIND_SW");
    DataPoint Score = DataPoint.getDataPointfromlabel("SCORE");

    /**
     * Main method of the smartControl component
     * Regulate the minecraft world
     * Used to adjust the setpoints according to the current world values
     * And forecast, as to get the best score possible
     */
    public void smartControlling() throws IOException {
        R_Solar_SW.setValue("true");
        R_Wind_SW.setValue("true");

        if (Float.parseFloat(Batt_p_left.getValue()) < 0.5f) {
            R_Factory_SP.setValue(String.valueOf(Float.parseFloat(R_Factory_SP.getValue()) - 0.05f));
            R_Coal_SP.setValue(String.valueOf(Float.parseFloat(R_Coal_SP.getValue()) + 0.05f));
        } else {
            R_Factory_SP.setValue(String.valueOf(Float.parseFloat(R_Factory_SP.getValue() + 0.05f)));
            R_Coal_SP.setValue(String.valueOf(Float.parseFloat(R_Coal_SP.getValue()) - 0.05f));
        }
        if (Float.parseFloat(R_Factory_SP.getValue()) > 1f) {
            R_Factory_SP.setValue("1f");
        }
        if (Float.parseFloat(R_Factory_SP.getValue()) < 0f) {
            R_Factory_SP.setValue("0f");
        }
        if (Float.parseFloat(R_Coal_SP.getValue()) > 1f) {
            R_Coal_SP.setValue("1f");
        }
        if (Float.parseFloat(R_Coal_SP.getValue()) < 0f) {
            R_Coal_SP.setValue("0f");
        }

        System.out.println("Score :" + Score.getValue());
    }
}
