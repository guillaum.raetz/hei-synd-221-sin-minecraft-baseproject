package ch.hevs.isi.database;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

/**
 * this class is used to send datapoint in database
 */
public class DatabaseConnector implements DataPointListener { //we implements the DPListner interface

    static DatabaseConnector instance = null;

    String PASSWORD = "be3c43afcd43d8905fd1b1edbd794021"; //md5 of "SIn30"
    String DATABASE = "SIn30";
    String USER = "SIn30";


    //constructor
    private DatabaseConnector() {
    }

    /**
     * Singleton: maximum one instance of DatabaseConnector
     *
     * @return instance i.e the object
     */
    public static DatabaseConnector getInstance() {//singleton
        if (instance == null) { //if there is no instance
            instance = new DatabaseConnector(); //we create once
        }
        return instance; //return the unique object
    }

    /**
     * push data in database
     *
     * @param label
     * @param value
     */
    private void pushToDB(String label, String value) {

        String finalurl = "https://influx.sdi.hevs.ch/write?db=" + DATABASE;
        String userpass = USER + ":" + PASSWORD;
        try {

            java.net.URL url = new URL(finalurl); //
            HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //
            String encoding = Base64.getEncoder().encodeToString(userpass.getBytes()); //
            connection.setRequestProperty("Authorization", "Basic " + encoding); //
            connection.setRequestProperty("Content-type", "binary/octet-stream"); //
            connection.setRequestMethod("POST"); //POST mean there is anything in the HTML trame body
            connection.setDoOutput(true); //
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream()); //
            String msg = label + " value=" + value;   //body of http + post request
            writer.write(msg); //
            writer.flush(); //
            writer.close(); //

            int responseCode = connection.getResponseCode(); //
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream())); //
            if (responseCode == 204) { //if the number start by 2 its correct, case 4: there is a problem
                while ((in.readLine()) != null) //consume one line
                {
                    // wait the end of reception
                }
            }
            connection.disconnect(); //Close all communication related objects
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * When a new value is added this method is called
     *
     * @param dp Datapoint to be added to database
     * @throws IOException
     */
    @Override
    public void onNewValue(DataPoint dp) throws IOException {
        pushToDB(dp.getLabel(), dp.getValue());
    }

}
