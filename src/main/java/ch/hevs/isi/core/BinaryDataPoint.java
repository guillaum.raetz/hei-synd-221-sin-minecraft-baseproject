package ch.hevs.isi.core;

import java.io.IOException;


/**
 * Class used to store binary data
 */
public class BinaryDataPoint extends DataPoint{
    private boolean value;

    /**
     * Constructor
     * @param label dataPoint name
     * @param isOutput if it's an output or input, for example the grid voltage is an input, and remote PV switch is an output
     */
    public BinaryDataPoint(String label, boolean isOutput)
    {
        super(label, isOutput);
    }

    /**
     * set the value of the datapoint
     * @param value value to set
     * @throws IOException
     * This method calls then the "onNewValue" method of WebConnector, Database and FielConnector
     * To write the new value on the webpage and database
     */
    public void setValue(boolean value) throws IOException {
        this.value=value;
        wc.onNewValue(this);
        DB.onNewValue(this);
        FC.onNewValue(this);
    }

    /**
     *  set the value of the datapoint
     * @param value value to set
     * @throws IOException
     * This method calls then the "onNewValue" method of WebConnector, Database and FielConnector
     * To write the new value on the webpage and database
     */
    public void setValue(String value) throws IOException {
        this.value = Boolean.parseBoolean(value);
        wc.onNewValue(this); //send the binary DP
        DB.onNewValue(this);
        FC.onNewValue(this);
    }

    /**
     * This method the boolean "value" as a String
     * @return true or false
     */
    public String getValue() //convert boolean to string
    {
        if(value){
            return "true";
        }
        else {
            return "false";
        }
    }
}
