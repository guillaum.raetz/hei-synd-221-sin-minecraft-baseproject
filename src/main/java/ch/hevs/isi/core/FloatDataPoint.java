package ch.hevs.isi.core;

import java.io.IOException;

/**
 * Class used to store float data
 */
public class FloatDataPoint extends DataPoint {

    private float value=0;

    /**
     * used to change the value contained in the DataPoint
     * Then calls WebConnector, DataBase and FieldConnector "onNewValue" method
     * as to update the value of the DataPoint in the previous class
     * @param val value to set in DataPoint
     */
    public void setValue(float val) throws IOException {
        this.value = val;
        wc.onNewValue(this);
        DB.onNewValue(this);
        FC.onNewValue(this);
    }

    /**
     * used to change the value contained in the DataPoint
     * Then calls WebConnector, DataBase and FieldConnector "onNewValue" method
     * as to update the value of the DataPoint in the previous class
     * @param val value to set in DataPoint
     */
    public void setValue(String val) throws IOException {

        this.value = Float.parseFloat(val);
        wc.onNewValue(this); //update of components
        DB.onNewValue(this);
        FC.onNewValue(this);
    }

    /**
     * Simple get method for value
     * @return value that is parsed to string
     */
    public String getValue()
    {
        return Float.toString(value);
    }

    /**
     * Constructor of FloatDataPoint
     * @param label name of the DataPoint
     * @param isOutput if it's an output or input, for example the grid voltage is an input, and remote PV switch is an output
     */
    public FloatDataPoint(String label, boolean isOutput)
    {
        super(label, isOutput);
    }
}
