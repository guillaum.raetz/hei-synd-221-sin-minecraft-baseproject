package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;

/**
 * Parent class of FloatDataPoint and BinaryDataPoint
 * Contain all the methods needed to create and use DataPoint objects
 * DataPoints are used to store the values of the minecraft World
 * And are used between all components (WEB,FIELD, DATABASE)
 */
public abstract class DataPoint {

    private String label;
    private boolean isOutput;

    /**
     * dataPointMap is a map of dataPoint and String
     * it is used to store all the datapoint needed for the minecraft world
     * It is static as we only use one in this project
     */
    private static HashMap <String, DataPoint> dataPointMap = new HashMap<>(); //map of DP, string -> DP

    /**
     * Used to get a datapoint from dataPointMap depending on its label
     * @param label name of the datapoint we want
     * @return the corresponding datapoint
     */
    public static DataPoint getDataPointfromlabel (String label)
    {
        return dataPointMap.get(label);
    }

    /**
     * Simple get method for label
     * @return label
     */
    public String getLabel()
    {
        return label;
    }

    /**
     * simple get method for isOutput
     * @return isOutput
     */
    public boolean isOutput()
    {
        return isOutput;
    } //is an output ? example button : true


    /**
     * Abstract method to get the value of the datapoint
     * Will be implemented in child class as it is different
     * between Float or Binary Datapoints
     * @return
     */
    public abstract String getValue(); //abstract : should be declared in child class

    /**
     * Abstract method to set the datapoint value
     * Will be implemented in child class as it is different
     * between Float or Binary Datapoints
     * @param val
     */
    public abstract void setValue(String val) throws IOException;

    WebConnector wc = WebConnector.getInstance();
    DatabaseConnector DB = DatabaseConnector.getInstance();
    FieldConnector FC = FieldConnector.get();

    /**
     * Constructor of Datapoint
     * Create a datapoint and add it to the datapoint map
     * @param label name of dataPoint
     * @param isOutput if it's an output or input, for example the grid voltage is an input, and remote PV switch is an output
     */
    protected DataPoint(String label, boolean isOutput)
    {
        this.label = label;
        this.isOutput = isOutput;
        dataPointMap.put(label, this);
    }

}
