package ch.hevs.isi.core;

import java.io.IOException;

/**
 * Interface implemented in class where Datapoints are used
 */
public interface DataPointListener {
    /**
     * Abstract method that we need to have when we use Datapoint in a class that implements this interface
     * @param dp the Float or Binary DataPoint
     * */
    void onNewValue(DataPoint dp) throws IOException;
}
