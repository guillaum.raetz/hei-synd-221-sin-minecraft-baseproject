package ch.hevs.isi.web;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * this class is used to establish a connection with a web page and transmit datapoint
 */
public class WebConnector extends WebSocketServer implements DataPointListener {

    static WebConnector instance = null; //objet connexion
    static final int PORT = 8888;

    // constructeur

    /**
     * the constructor create a new connection on the 8888 port and start the server
     */
    private WebConnector() {
        super(new InetSocketAddress(PORT)); //on hérite du constructeur de WebSocketServeur
        this.start(); //lance le serveur, rend la connexion possible avec les clients
    }

    /**
     * used to display errors relating to communication
     *
     * @param webSocket
     * @param e
     */
    @Override
    public void onError(WebSocket webSocket, Exception e) {
        e.printStackTrace();
    }
    //au lancement du serveur

    /**
     * start the server and display the message "server started" in the console
     */
    @Override
    public void onStart() {
        System.out.println("serveur started");
    }

    /**
     * send the value on the Web at the address given by the label
     *
     * @param label
     * @param value
     * @throws IOException
     */
    private void pushToWebPages(String label, String value) throws IOException {
        this.broadcast(label + "=" + value);
        System.out.println("Write on the web in the label: " + label + " the value: " + value);
    }

    /**
     * interrupt that is triggered when a client connects,
     * a message is written on the console and another on the web
     *
     * @param webSocket       used for the connection
     * @param clientHandshake not used here
     */
    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        this.addConnection(webSocket); //on ajoute le client à la liste de connection
        System.out.println("Client connected to server"); //message écrit sur la console à la connexion du client
        webSocket.send("Welcome to server web"); //message écrit sur le client à la connexion

    }

    /**
     * interrupt that triggers when the client terminates the connection
     *
     * @param webSocket used for the connection
     * @param i
     * @param s
     * @param b
     */
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        System.out.println("GoodBye"); //written when the client disconnects
        this.removeConnection(webSocket);

    }


    /**
     * interrupt when its happen anything in input on the web
     * separates the value and the label
     * transmits changes on the client to all other clients
     *
     * @param webSocket for the connection
     * @param s         the received value
     */
    @Override
    public void onMessage(WebSocket webSocket, String s) {

        String[] message = s.split("="); //split the chain of char by the char "=" and stock in a table of chain of char

        if (message.length == 2) {
            String label = message[0];
            String value = message[1];

            DataPoint dp = DataPoint.getDataPointfromlabel(label);
            //put the value in datapoint if its a float
            if (dp != null) {

                try {
                    float f;
                    if (!value.equals("true") && !value.equals("false")) {
                        f = Float.parseFloat(value);
                    }
                    dp.setValue(value);
                } catch (NumberFormatException | IOException nfe) {
                    System.out.println("INVALID value decoded: " + value);
                }
            } else {
                System.out.println("No DP found for label " + label);
            }
        } else {
            System.out.println("INVALID message: " + s);
        }
/*
        try {
            pushToWebPages("GRID_U_FLOAT","252");
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        this.broadcast(message[0] + "=" + message[1]); //send sur tous les clients
    }


    /**
     * Singleton: maximum one instance of WebConnector
     *
     * @return the object instance
     */
    public static WebConnector getInstance() {
        if (instance == null) {
            instance = new WebConnector();
        }
        return instance;
    }

    /**
     * used to send a datapoint on the Web
     *
     * @param DP
     */
    @Override
    public void onNewValue(DataPoint DP) { //write a datapoint in webpage
        try {
            pushToWebPages(DP.getLabel(), DP.getValue());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //for test
    public static void main(String[] args) {
        BinaryDataPoint bdp = new BinaryDataPoint("REMOTE_WIND_SW", true);
        FloatDataPoint fdp = new FloatDataPoint("REMOTE_COAL_SP", true);
        WebConnector wc = WebConnector.getInstance();
    }


}
