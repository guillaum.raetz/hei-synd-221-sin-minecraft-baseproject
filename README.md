Explication du projet:


Le projet sert à réguler un monde minecraft avec le mode Electrical Age. Ce monde est composé de producteurs et consommateurs d'électricité. Une journée dure 20 minutes. Le but est de lire les valeurs électriques sur minecraft et de les réguler. Il y a aussi un stockage des valeurs importantes dans une base de données nommée influxdb. Cette base de données fonctionne en synergie avec l'outil de visualisation en temps réel Graphana. Nous avons aussi implémenté un serveur Web sur intellij pour communiquer avec des pages HTTP clientes. Le but est là d'afficher les valeurs mais aussi de renvoyer au serveur des commandes grâce à des boutons en entrée sur le web. Ces boutons fonctionnent parallèlement à ceux dans le monde minecraft.
Notre projet est codé sur intellij Idea. Il est composé de 6 parties. Le package core contient les datapoints. Le package modbus et field permet de lire les valeurs sur minecraft. Le package "smartcontrol" sert à renvoyer des valeurs sur minecraft pour réguler le réseau électrique. Le package Web contient uniquement la classe WebConnector qui permet la communication entre le serveur que l'on crée et la page web cliente. Sans oublier le package Database contenant la classe DatabaseConnector dont l'utilité a déjà été mentionnée.  

Comment lancer le code:


-Charger le projet complet sur l'IDE intellij IDEA
-Run la méthode main de la classe ModbusAccessor dans le package nommé Field